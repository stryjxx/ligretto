package pl.wroc.pwr.ligretto.GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;


import pl.wroc.pwr.ligretto.model.Card;
import pl.wroc.pwr.ligretto.model.Player;

public class PlayerPanel extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = -1048132179015573561L;
	
	protected Player player;
	
	protected Card currentCard;
	protected int indexOfHand;
	protected Integer indexOfRow;
	
	protected ArrayList<JButton> rowButtons;
	protected JButton handButton;
	
	
	public PlayerPanel(Player player){
		
		this.player = player;
		rowButtons = new ArrayList<JButton>();
		for(int i =0 ; i<3; i++){		
			Card card = player.getCardSet().getCardRow().get(i);
			JButton row = new JButton(card.toString());
			row.setBackground(card.getColor());
			row.setForeground(Color.WHITE);
			row.setSize(100, 100);
			row.addActionListener(this);
			this.add(row);
			rowButtons.add(row);			
		}
		
		this.add(Box.createRigidArea(new Dimension(10, 10)));
		this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
	
		indexOfRow = null;
		indexOfHand = 0;
		Card card = player.getCardSet().getCardHand().get(indexOfHand);
		handButton = new JButton(card.toString());
		handButton.setBackground(card.getColor());
		handButton.setForeground(Color.WHITE);
		handButton.setSize(100, 100);
		handButton.addActionListener(this);
		
		this.add(handButton);
		
		
			
		}

	public Card takeCurrentCard(){
		if(this.currentCard != null){
			if(this.indexOfRow==null){
				this.player.getCardSet().getCardHand().remove(this.indexOfHand);
				}
			}
		return currentCard;
	}
	

	
	@Override
	public void actionPerformed(ActionEvent e) {
		
	}

	public Integer getIndexOfRow() {
		return indexOfRow;
	}

	public void setIndexOfRow(Integer indexOfRow) {
		this.indexOfRow = indexOfRow;
	}
	
		
	}
	
	



