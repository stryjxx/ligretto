package pl.wroc.pwr.ligretto.model;

import java.awt.Color;

public class Card implements Comparable<Card> {

	private int number;
	
	private Color color;
	
	public Card(){};
	
	public Card(int n, Color c)
	{
		this.number = n;
		this.color = c;
	}
	
	//private String symbol;
	
	public int getNumber(){
		return number;
	}
	
	public void setNumber(int n){
		this.number = n;
	}
	
	public Color getColor(){
		return color;
	}
	
	public void setColor(Color c){
		this.color = c;
	}
	

	@Override
	public int compareTo(Card o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public String toString(){

		return "" +  this.number;
	}
}
