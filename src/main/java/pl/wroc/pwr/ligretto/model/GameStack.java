package pl.wroc.pwr.ligretto.model;

import java.util.Stack;

public class GameStack {
	
	private Stack<Card> cards;
	private String color;
	
	public GameStack()
	{
		cards = new Stack<Card>();
	}
	
	public Stack<Card> getCards(){
		return this.cards;
	}
	public void setCards(Stack<Card> c){
		this.cards = c;
	}
	
	public String getColor(){
		return this.color;
	}
	public void setColor(String c){
		this.color = c;
	}
	
	public void showGameStack(){
		if(cards.empty() == true)
			System.out.print("[0]");
		else
		{
			Card c = cards.peek();
			System.out.print("["+c.toString()+"]");
		}
	}

}
