package pl.wroc.pwr.ligretto.GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Console;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;








import pl.wroc.pwr.ligretto.game.Game;
import pl.wroc.pwr.ligretto.model.Card;
import pl.wroc.pwr.ligretto.model.Player;

public class LigrettoGUI {
	
	public JFrame frame = new JFrame();
	
	public LigrettoGUI(){
	}
	
	public void addPlayerPanel(Player player, int size)
	{
		if(size == 1)
		{
			PlayerPanel realPlayer = new RealPlayerPanel(player);
			realPlayer.setLayout(new BoxLayout(realPlayer, BoxLayout.X_AXIS));
			frame.getContentPane().add(BorderLayout.SOUTH, realPlayer);
		}		
		else if(size == 2)
		{
			PlayerPanel realPlayer = new RealPlayerPanel(player);
			realPlayer.setLayout(new BoxLayout(realPlayer, BoxLayout.Y_AXIS));
			frame.getContentPane().add(BorderLayout.EAST, realPlayer);
		}		
		else if(size == 3)
		{
			PlayerPanel realPlayer = new RealPlayerPanel(player);
			realPlayer.setLayout(new BoxLayout(realPlayer, BoxLayout.X_AXIS));
			frame.getContentPane().add(BorderLayout.NORTH, realPlayer);
		}		
		else if(size == 4)
		{
			PlayerPanel realPlayer = new RealPlayerPanel(player);
			realPlayer.setLayout(new BoxLayout(realPlayer, BoxLayout.Y_AXIS));
			frame.getContentPane().add(BorderLayout.WEST, realPlayer);
		}
		frame.invalidate();
		frame.validate();
		frame.repaint();	
	}
	public void createGUI() {	
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 500);
		frame.setTitle("Ligretto");
		

//		PlayerPanel compPlayer1 = new PlayerPanel(game.getPlayers().get(1));
//		compPlayer1.setLayout(new BoxLayout(compPlayer1, BoxLayout.X_AXIS));
//		PlayerPanel compPlayer2 = new PlayerPanel( game.getPlayers().get(2));
//		compPlayer2.setLayout(new BoxLayout(compPlayer2, BoxLayout.Y_AXIS));
//		PlayerPanel compPlayer3 = new PlayerPanel(game.getPlayers().get(3));
//		compPlayer3.setLayout(new BoxLayout(compPlayer3, BoxLayout.Y_AXIS));
		
//		frame.getContentPane().add(BorderLayout.SOUTH, realPlayer);
//		frame.getContentPane().add(BorderLayout.NORTH, compPlayer1);
//		frame.getContentPane().add(BorderLayout.EAST, compPlayer2);
//		frame.getContentPane().add(BorderLayout.WEST, compPlayer3);

		Board board = new Board();
		frame.getContentPane().add(BorderLayout.CENTER, board);
		
		frame.setVisible(true);
	}

//	@Override
//	public void actionPerformed(ActionEvent e) {
//		System.out.println("Button clicked");
//		JButton button = (JButton) e.getSource();
//		if(this.board.getStackButtons().contains(button)){
//			Card card = this.realPlayer.takeCurrentCard();
//			if(card != null){
//				button.setText(card.toString());
//				button.setBackground(card.getColor());
//				realPlayer.setCurrentCard(null);
//			}
//		}
//	}
}
