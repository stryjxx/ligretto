package pl.wroc.pwr.ligretto.game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import pl.wroc.pwr.ligretto.GUI.LigrettoGUI;
import pl.wroc.pwr.ligretto.model.Card;
import pl.wroc.pwr.ligretto.model.ComputerPlayer;
import pl.wroc.pwr.ligretto.model.GameStack;
import pl.wroc.pwr.ligretto.model.Player;
import pl.wroc.pwr.ligretto.model.RealPlayer;

public class Game {

	
	private ArrayList<Player> players;
	private ArrayList<GameStack> gameStacks;
	private LigrettoGUI gui;
	
	private Map<Socket, PrintWriter> clients = new HashMap<Socket, PrintWriter>();
	
	public Game() {
		players = new ArrayList<Player>();
		gameStacks = new ArrayList<GameStack>();
		for (int i = 0; i < 16; i++) {
			  gameStacks.add(new GameStack());
		}
		gui = new LigrettoGUI();
	}
	
	public static void main(String[] args) {
		Game game = new Game();
		game.gui.createGUI();
		game.start();
		
	//	Player realPlayer = new RealPlayer();
		
	//	Player comPlayer1 = new ComputerPlayer();
	//	Player comPlayer2 = new ComputerPlayer();
	//	Player comPlayer3 = new ComputerPlayer();
		
	//	game.addPlayer(realPlayer);
	//	game.addPlayer(comPlayer1);
	//	game.addPlayer(comPlayer2);
	//	game.addPlayer(comPlayer3);
				
		//LigrettoGUI gui = new LigrettoGUI(game);
		//gui.createGUI();
		

		//game.play();

	}
	
	private void start() {
		ServerSocket serverSocket = getServerSocket();
		while (serverSocket != null && !serverSocket.isClosed()) {
			Socket clientSocket = acceptConnection(serverSocket);
			createClientManagerThread(clientSocket);
			System.out.println("Skonfigurowano po\u0142\u0105czenie z klientem");
		}
	}
	
	private ServerSocket getServerSocket() {
		try {
			return new ServerSocket(5014);
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	private Socket acceptConnection(ServerSocket serverSocket) {
		Socket clientSocket = null;
		try {
			clientSocket = serverSocket.accept();
			PrintWriter writer = new PrintWriter(clientSocket.getOutputStream());
			clients.put(clientSocket, writer);
			Player newPlayer = new Player();
			players.add(newPlayer);
			gui.addPlayerPanel(newPlayer, players.size());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return clientSocket;
	}
	
	private void createClientManagerThread(Socket clientSocket) {
		Thread clientManagement = new Thread(new ClientManager(clientSocket));
		clientManagement.start();		
	}
	
	public class ClientManager implements Runnable {
		BufferedReader reader;
		Socket socket;
		
		public ClientManager(Socket clientSocket) {
			try {
				socket = clientSocket;
				InputStreamReader isReader = new InputStreamReader(socket.getInputStream());
				reader = new BufferedReader(isReader);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		@Override
		public void run() {
			String message;
			while ((message = getNextLineFromSocket()) != null) {
				System.out.println("Odczytano: " + message);
				broadcast(message);
			}
		}

		public void broadcast(String message) {
			for (PrintWriter clientWriter : clients.values()) {
				clientWriter.println(message);
				clientWriter.flush();
			}
		}
	
		public String getNextLineFromSocket() {
			String message = null;
			try {
				message = reader.readLine();
			} catch (SocketException e) {
				System.out.println("Utracono po\u0142\u0105czenie z klientem");
				clients.remove(socket);
				System.out.println("Liczba klient\u00f3w: " + clients.size());
				// socket jest juÅ¼ zamkniÄ™ty
			} catch (IOException e) {
				e.printStackTrace();
			}
			return message;
		}
	}

	public ArrayList<Player> getPlayers() {
		return players;
	}
	
	public ArrayList<GameStack> getGameStacks() {
		return gameStacks;
	}

	public void play() {
		System.out.println("Welcome to Ligretto!");
		//players.get(0).showHand();
		displayTable();
//		players.get(0).showStack();
//		players.get(0).showRow();

	}

	public void addPlayer(Player player) {
		this.players.add(player);	
	}
	
	public void displayTable(){
		System.out.println("Plansza:");
		for(GameStack gs : gameStacks){
			gs.showGameStack();
		}
		System.out.println();
	}
	

	
}
