package pl.wroc.pwr.ligretto.model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

public class CardSet {
	
	private ArrayList<Card> cards;
	private ArrayList<Card> cardHand;
	private Stack<Card> cardStack;
	private ArrayList<Card> cardRow;

	public CardSet()
	{
		cards = new ArrayList<Card>();
		cardStack = new Stack<Card>();
		cardRow = new ArrayList<Card>();
		cardHand = new ArrayList<Card>();
		
		for(int i = 1; i <= 10; i++)
		{
			
			cards.add(new Card(i,Color.BLUE));
			cards.add(new Card(i, Color.GREEN));
			cards.add(new Card(i, Color.YELLOW));
			cards.add(new Card(i, Color.RED));
		}
		shuffle();
		cardHand = new ArrayList<Card>();
		for(int i = 1; i<= 27; i++)
		{
			cardHand.add(cards.remove(0));	
		}
		
		for(int i = 1; i<= 10; i++)
		{
			cardStack.add(cards.remove(0));
		}
		
		for(int i = 1; i<= 3; i++)
		{
			cardRow.add(cards.remove(0));	
		}
		
	}
	
	public void shuffle() {
		Collections.shuffle(this.cards);
	}
	

	public Card drawRow(int i) {
		if(!cardStack.empty()){
			cardRow.set(i, cardStack.pop());
			return cardRow.get(i);
		}
		else return null;
	}
	
	public void shuffleHand() {
		
		Collections.shuffle(this.cardHand);
		
	}
	
	public ArrayList<Card> getCardHand() {
		return cardHand;
	}
	public void setCardHand(ArrayList<Card> ch) {
		this.cardHand = ch;
	}
	
	public Stack<Card> getCardStack() {
		return cardStack;
	}
	public void setCardStack(Stack<Card> cs) {
		this.cardStack= cs;
	}
	
	public ArrayList<Card> getCardRow() {
		return cardRow;
	}
	public void setCardRow(ArrayList<Card> cr) {
		this.cardRow = cr;
	}
	
	public ArrayList<Card> getCards() {
		return cards;
	}
	public void setCards(ArrayList<Card> c) {
		this.cards = c;
	}	
}
