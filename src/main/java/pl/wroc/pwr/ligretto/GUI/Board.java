package pl.wroc.pwr.ligretto.GUI;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

public class Board extends JPanel {


	private static final long serialVersionUID = 7359980952878004103L;
	


	private ArrayList<JButton> stackButtons;


	public Board() {
		stackButtons = new ArrayList<JButton>();
		
		drawBoard();
	}

	private void drawBoard() {
		this.setLayout(new GridLayout(4, 4));
		this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				JButton GameStack = new JButton();
				GameStack.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
				GameStack.setForeground(Color.BLACK);
				GameStack.setText("O");
				this.add(GameStack);
				stackButtons.add(GameStack);
			}
		}		
	}
	
	public ArrayList<JButton> getStackButtons() {
		return stackButtons;
	}

	public void setStackButtons(ArrayList<JButton> stackButtons) {
		this.stackButtons = stackButtons;
	}
}
