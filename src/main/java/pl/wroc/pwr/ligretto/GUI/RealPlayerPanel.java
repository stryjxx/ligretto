package pl.wroc.pwr.ligretto.GUI;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.JButton;


import pl.wroc.pwr.ligretto.model.Card;
import pl.wroc.pwr.ligretto.model.Player;

public class RealPlayerPanel extends PlayerPanel {
	

	private static final long serialVersionUID = -3620420654505347532L;

	public RealPlayerPanel(Player player){
		super( player);
	}
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton button = (JButton) e.getSource();
		if (button == handButton){
			this.indexOfHand += 3;
			if(player.getCardSet().getCardHand().size()<=1){
				button.setBackground(Color.GRAY);
				button.setText("X");
				button.setEnabled(false);
			}
			else {
				if(indexOfHand >= player.getCardSet().getCardHand().size()){
					this.indexOfHand = 0;
					player.getCardSet().shuffleHand();
				}
				this.currentCard = player.getCardSet().getCardHand().get(indexOfHand);
				button.setText(currentCard.toString());
				button.setBackground(currentCard.getColor());
			}
		}
		else if (rowButtons.contains(button)){
			int i = rowButtons.indexOf(button);
			Card card = player.getCardSet().drawRow(i);
			if(card == null){
				button.setBackground(Color.GRAY);
				button.setText("X");
				button.setEnabled(false);
			}
			else{
				button.setText(card.toString());
				button.setBackground(card.getColor());
			}
		}
	
		button.repaint();
		
	}
	

}
