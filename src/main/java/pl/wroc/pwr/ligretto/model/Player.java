package pl.wroc.pwr.ligretto.model;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import pl.wroc.pwr.ligretto.GUI.LigrettoGUI;

public class Player {
	private static final int PORT = 5014;
	private static final String HOST = "127.0.0.1";
	private BufferedReader reader;
	private PrintWriter writer;
	private Socket socket;
	public LigrettoGUI gui;
	
	private CardSet cardSet;
	
	public Player()
	{
		cardSet = new CardSet();
	}
	
	public static void main(String[] args) {
		Player player = new Player();
		player.run();
	}
	
	protected void configureCommunication() throws IOException {
		socket = new Socket(HOST, PORT);
		InputStreamReader isr = new InputStreamReader(socket.getInputStream());
		reader = new BufferedReader(isr);
		
		writer = new PrintWriter(socket.getOutputStream());
		
		System.out.println("Polaczono z serwerem.");
	}
	
	protected void startReceiverThread() {
		Thread receiver = new Thread(new MessageReceiver());
		receiver.start();		
	}
	
	protected void run() {
		//frame = createFrame();
		try {
			configureCommunication();
			startReceiverThread();
			//showFrame();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Nie udalo sie polczyc z serwerem. Blad polaczenia");
			System.exit(-1);
		}	
	}
	
	public CardSet getCardSet() {
		return cardSet;
	}
	
	public void setCardSet(CardSet cs) {
		this.cardSet = cs;
	}

//	public void showHand(){
//		System.out.println("Reka: ");
//		for(Card c : cardSet.getCardHand()){
//			System.out.print(c.toString() + ", ");
//		}
//		System.out.println();
//	}
//	
//	public void showRow(){
//		System.out.println("Rzad: ");
//		for(Card c : cardSet.getCardRow()){
//			System.out.print(c.toString()+ ", ");
//		}
//		System.out.println();
//	}
//	
//	public void showStack(){
//		System.out.println("Stos: ");
//		Card c = cardSet.getCardStack().peek();
//		System.out.print("["+c.toString()+"]");
//		System.out.println();
//	}
	
	public class MessageReceiver implements Runnable {

		@Override
		public void run() {
			String message;
				try {
					while ((message = reader.readLine()) != null) {
						System.out.println("Odczytano: " + message);
						//receivedMessages.append(message);
						//receivedMessages.append("\n");
					}
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("Utacono polaczenie z serwerem. Blad polaczenia");
					System.exit(-1);
				}
		}
		
	}
	
	public Object readObjectFromSocket() {
		Socket socket = null;
		InputStream inputStream = null;
		ObjectInputStream objectInputStream = null;
		Object object = null;
		try {
			socket = new Socket(HOST, PORT);
			inputStream = socket.getInputStream();
			objectInputStream = new ObjectInputStream(inputStream);
			object = objectInputStream.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			closeResource(objectInputStream);
			closeResource(inputStream);
			closeResource(socket);
		}
		return object;
	}
	
	public void writeObjectToSocket(Object objectToWrite) {
		Socket socket = null;
		OutputStream outputStream = null;
		ObjectOutputStream objectOutputStream = null;
		try {
			socket = new Socket(HOST, PORT);
			outputStream = socket.getOutputStream();
			objectOutputStream = new ObjectOutputStream(outputStream);
			objectOutputStream.writeObject(objectToWrite);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			closeResource(objectOutputStream);
			closeResource(outputStream);
			closeResource(socket);
		}
	}
	
	private static void closeResource(Closeable closeableResource) {
		if (closeableResource != null) {
			try {
				closeableResource.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
}
